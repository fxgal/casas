<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contactos Controller
 *
 * @property \App\Model\Table\ContactosTable $Contactos
 */
class ContactosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

     public function info()
     {
         $user=$this->Auth->user();
         $contactos = $this->Contactos->find('all')->where(['persona_id'=>$user->persona->id]);
         $this->set(compact('contactos'));
         $this->set('_serialize', ['contactos']);
     }
    public function index()
    {
        $contactos = $this->paginate($this->Contactos);

        $this->set(compact('contactos'));
        $this->set('_serialize', ['contactos']);
    }

    /**
     * View method
     *
     * @param string|null $id Contacto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        return $this->redirect(['action' => 'info',$id]);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $user=$this->Auth->user();
        $id=$id?$id:$user->persona->id;
        $contacto = $this->Contactos->newEntity();
        if ($this->request->is('post')) {
            $contacto = $this->Contactos->patchEntity($contacto, $this->request->data);
            $contacto->persona_id=$id;
            if ($this->Contactos->save($contacto)) {
                $this->Flash->success(__('Datos guardados.'));

                return $this->redirect(['action' => 'info',$user->persona->id]);
            } else {
                $this->Flash->error(__('Ha ocurrido un error.'));
            }
        }
        $this->set(compact('contacto'));
        $this->set('_serialize', ['contacto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contacto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contacto = $this->Contactos->get($id, [
            'contain' => ['Personas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contacto = $this->Contactos->patchEntity($contacto, $this->request->data);
            if ($this->Contactos->save($contacto)) {
                $this->Flash->success(__('Datos actualizados.'));

                return $this->redirect(['action' => 'info',$contacto->persona_id]);
            } else {
                $this->Flash->error(__('Ha ocurrido un error.'));
            }
        }
        $this->set(compact('contacto'));
        $this->set('_serialize', ['contacto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contacto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contacto = $this->Contactos->get($id);
        if ($this->Contactos->delete($contacto)) {
            $this->Flash->success(__('The contacto has been deleted.'));
        } else {
            $this->Flash->error(__('The contacto could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'info']);
    }
}
