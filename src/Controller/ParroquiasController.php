<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Parroquias Controller
 *
 * @property \App\Model\Table\ParroquiasTable $Parroquias
 */
class ParroquiasController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow(['registro', 'logout','login','index','add']);
        $this->Auth->allow();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Municipios']
        ];
        $parroquias = $this->paginate($this->Parroquias);

        $this->set(compact('parroquias'));
        $this->set('_serialize', ['parroquias']);
    }

    public function lista($id = null)
    {
        $this->viewBuilder()->layout('ajax');
        $parroquias = $this->getParroquias($id);
        $this->set('parroquias', $parroquias);
    }

    /**
     * View method
     *
     * @param string|null $id Parroquia id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $parroquia = $this->Parroquias->get($id, [
            'contain' => ['Municipios', 'Personas']
        ]);

        $this->set('parroquia', $parroquia);
        $this->set('_serialize', ['parroquia']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $parroquia = $this->Parroquias->newEntity();
        if ($this->request->is('post')) {
            $parroquia = $this->Parroquias->patchEntity($parroquia, $this->request->data);
            if ($this->Parroquias->save($parroquia)) {
                $this->Flash->success(__('The parroquia has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The parroquia could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->Parroquias->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('parroquia', 'municipios'));
        $this->set('_serialize', ['parroquia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Parroquia id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $parroquia = $this->Parroquias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $parroquia = $this->Parroquias->patchEntity($parroquia, $this->request->data);
            if ($this->Parroquias->save($parroquia)) {
                $this->Flash->success(__('The parroquia has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The parroquia could not be saved. Please, try again.'));
            }
        }
        $municipios = $this->Parroquias->Municipios->find('list', ['limit' => 200]);
        $this->set(compact('parroquia', 'municipios'));
        $this->set('_serialize', ['parroquia']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Parroquia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $parroquia = $this->Parroquias->get($id);
        if ($this->Parroquias->delete($parroquia)) {
            $this->Flash->success(__('The parroquia has been deleted.'));
        } else {
            $this->Flash->error(__('The parroquia could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
