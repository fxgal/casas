<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('RecImagen');
        $this->loadComponent('Switch');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']
                ]
            ],
            'authError' => 'Did you really think you are allowed to see that?',
            'loginRedirect' => [
                'controller' => 'personas',
                'action' => 'info'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
            ]
        ]);
    }

//Variables para manejo de accesos
private $controllers=[
        'Contactos',
        'Documentos',
        'Estados',
        'Fotospersonas',
        'Municipios',
        'Parroquias',
        'Personas',
        'Personas_rols',
        'Rols',
        'Users',
    ];
private $permisos=[
        'Administrador'=>[],
        'Temporal'=>[
            'Personas'=>['add']
        ],
        'Usuario'=>[
                'Contactos'=>[
                    'add','delete','edit','view','info'
                ],
                'Estados'=>[
                    'lista'
                ],
                'Fotospersonas'=>[
                    'add','delete','edit','view'
                ],
                'Municipios'=>[
                    'lista'
                ],
                'Parroquias'=>[
                    'lista'
                ],
                'Personas'=>[
                    'add','edit','view','info'
                ],
                'Users'=>[
                    'add','edit','view'
                ],
            ]
    ];
    public function isAuthorized($user)
    {
        $controller = $this->request->params['controller'];
        $action = $this->request->params['action'];
        $permisos = $this->permisos;
        //debug($user);
        $rol=$user->rol->nombre;
        if ($rol=='Administrador') {
            return true;
        }else{
            if(array_key_exists($rol,$permisos)){
                if(array_key_exists($controller,$permisos[$rol])){
                    if (in_array($action,$permisos[$rol][$controller])) {
                        return true;
                    }
                }
            }
        }
        $this->Flash->error(__('Acceso denegado'));
        return false;
    }

    public function beforeFilter(Event $event) {
        $this->Auth->allow(['login', 'logout','reset']);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function getEstados(){
    $this->loadModel('Estados');
    $datos = $this->Estados->find('all', ['fields'=>['id','estado']]);
    foreach ($datos as $dato) {
      $estados[$dato->id]=$dato->estado;
    }
    return $estados;
  }
  public function getMunicipios($estado=1){
    $this->loadModel('Municipios');
    $datos = $this->Municipios->find('all', [
        'fields'=>['id','municipio'],
        'conditions'=>[
          'Municipios.estado_id'=>$estado
        ]
      ]
    );
    foreach ($datos as $dato) {
      $municipios[$dato->id]=$dato->municipio;
    }
    if (empty($municipios)) {
      return "Ha ocurrido un error";
    }
    return $municipios;
  }
  public function getParroquias($municipio=1){
    $this->loadModel('Parroquias');
    $datos = $this->Parroquias->find('all', [
        'fields'=>['id','parroquia'],
        'conditions'=>[
          'Parroquias.municipio_id'=>$municipio
        ]
      ]
    );
    foreach ($datos as $dato) {
      $parroquias[$dato->id]=$dato->parroquia;
    }
    if (empty($parroquias)) {
      return "Ha ocurrido un error";
    }
    return $parroquias;
  }

  public function getRols($id=null){
    $this->loadModel('Rols');
    if(!$id)
        $datos = $this->Rols->find('all', ['fields'=>['id','nombre'],'conditions'=>['id'>0]]);
    else
        $datos = $this->Rols->find('all', ['fields'=>['id','nombre'],'conditions'=>['id'=>$id]]);

    foreach ($datos as $dato) {
      $rols[$dato->id]=$dato->nombre;
    }
    return $rols;
  }

}
