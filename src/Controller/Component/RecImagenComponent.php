<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
/**
 * Por Félix Galindo
 * Componente usado para recortar imagenes con jcrop y generar thumbnail
 *
 */
class RecImagenComponent extends Component
{
    public function recortarImagen($imagen, $x, $y, $w, $h,$dir, $min = false){
      if (!empty($imagen['tmp_name'])) {
        if(!file_exists(WWW_ROOT. 'pictures'. DS .$dir))
            mkdir(WWW_ROOT. 'pictures'. DS .$dir, 0766, true);
        $src=$imagen['tmp_name'];
        $jpeg_quality = 100;
        $img_r = imagecreatefromjpeg($src);
        //--Validacion y tamaño
        $x=empty($x)?0:$x;
        $y=empty($y)?0:$y;
        $w=empty($w)?imagesx($img_r):$w;
        $h=empty($h)?imagesy($img_r):$h;
        if ($w==$h){
          $targ_w = $targ_h = 768;
        }else{
          $targ_w = $w;
          $targ_h = $h;
        }
         //
         $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
         imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
         $nombre=time().rand(1000,10000000);
         $url= 'pictures'.DS. $dir . DS.md5($nombre).'.jpg';
         $imagen_recortada = WWW_ROOT . $url;
         imagejpeg($dst_r, $imagen_recortada, $jpeg_quality);
         if($min){
           $urlmin=$this->recortarImagenMin($src,$nombre,$x,$y,$w,$h,$dir);
           return [$url,$urlmin];
         }
         return $url;
      }
    }

    public function recortarImagenMin($src = null, $nombre, $x=0, $y=0,$w=0,$h=0, $dir){
      if (!empty($src)) {
        if ($w>=2500) {
          $div=8;
        }elseif ($w>=1000 && $w<2500) {
          $div=6;
        }elseif ($w>=500 && $w<1000) {
          $div=4;
        }elseif ($w>=200 && $w<500) {
          $div=2;
        }else {
          $div=1;
        }
         $targ_w = round(($w/$div));
         $targ_h = round(($h/$div));
         $jpeg_quality = 100;
         $img_r = imagecreatefromjpeg($src);
         $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
         imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
         $imagen_recortada = WWW_ROOT . 'pictures'.DS. $dir. DS.md5($nombre).'.min.jpg';
         $url= 'pictures'.DS. $dir . DS.md5($nombre).'.min.jpg';
         imagejpeg($dst_r, $imagen_recortada, $jpeg_quality);
         return $url;
      }
    }
}



 ?>
