<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
/**
 * Por Félix Galindo
 * Componente usado para activar o desactivar registros
 *
 */
class SwitchComponent extends Component
{
    public function active($tabla,$id){
        $data = TableRegistry::get($tabla);
        $query = $data->query();
        $query
          ->update()
          ->set(['active' => true])
          ->where(['id' => $id])
          ->execute();
    }
    public function inactive($tabla,$id){
        $data = TableRegistry::get($tabla);
        $query = $data->query();
        $query
          ->update()
          ->set(['active' => false])
          ->where(['id' => $id])
          ->execute();
    }
    public function change($tabla,$where){
        $data = TableRegistry::get($tabla);
        $query = $data->query();
        $query
          ->update()
          ->set(['active' => false])
          ->where(['id' => $id])
          ->execute();
    }
}



 ?>
