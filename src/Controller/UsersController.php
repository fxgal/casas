<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function login()
    {
        #$this->viewBuilder()->layout('login');
        $loguser = $this->request->session()->read('Auth.User');

        if(!empty($loguser)) return $this->redirect($this->Auth->redirectUrl());
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $user = $this->Users->get($user['id'], [
                    'contain' => ['Rols','Personas'=>function($q){
                        return $q->contain([
                            'Contactos',
                            'Fotospersonas'=>function($q){
                            return $q
                                ->where(['active'=>true]);
                        }]);
                    }]
                ]);
                $this->Auth->setUser($user);
                $this->setAcceso($user['id']);
                if(!$this->request->session()->read('Auth.User.persona.nombre')) {
                    $this->Flash->warning(__('Debes agregar tus datos personales'));
                    return $this->redirect(['controller'=>'personas','action' => 'add']);
                }else{
                    $this->Flash->success(__('Bienvenido '.$this->request->session()->read('Auth.User.persona.nombre')));
                    return $this->redirect(['controller'=>'personas','action'=>'info', $user->persona->id]);
                }
            }
            $this->Flash->error(__('Correo o clave incorrectos. Intente de nuevo.'));
        }
    }

    public function logout()
    {
        $this->Flash->success(__('Ha cerrado sesión'));
        return $this->redirect($this->Auth->logout());
    }


    public function setAcceso($primaryKey) {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->get($primaryKey);
        $user->access = date('Y-m-d H:i:s');
        $usersTable->save($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->Users->find('all')->contain(['Personas','Rols']);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [
                'Personas'=>function($q){
                    return $q->contain(['Rols','Estados','Municipios','Parroquias']);
                }]
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function perfil()
    {
        $id=$this->request->session()->read('Auth.User.id');
        $user = $this->Users->get($id, [
            'contain' => [
                'Personas'=>function($q){
                    return $q->contain(['Rols','Estados','Municipios','Parroquias']);
                }]
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->rol_id=empty($user->rol_id)?2:$user->rol_id;
            $user = $this->Users->save($user);
            if ($user) {
                $this->Flash->success(__('Ahora registre los datos personales'));
                return $this->redirect(['controller'=>'personas','action' => 'add',$user->id]);
            } else {
                $this->Flash->error(__('Lo siento, ha ocurrido un error.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function registro()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user = $this->Users->save($user);
            if ($user) {
                $this->Flash->success(__('Ahora debes iniciar tu sesión'));
                return $this->redirect(['controller'=>'users','action' => 'login']);
            } else {
                $this->Flash->error(__('Lo siento, ha ocurrido un error.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function reset()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user= $this->Users->find('All')->where(['email'=>$this->request->data['email']]);
            $user = $user->first();
            if($user){
                $user = $this->Users->patchEntity($user, $this->request->data);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Ingrese con su nueva clave.'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('Ha ocurrido un error.'));
                }
            }else{
                $this->Flash->error(__('Usuario no encontrado.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
