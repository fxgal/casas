<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Personas Controller
 *
 * @property \App\Model\Table\PersonasTable $Personas
 */
class PersonasController extends AppController
{
    public function info($id = null){
        if(!$id) $id=$this->Auth->user('id');
        $persona = $this->Personas->get($id,[
            'conditions'=>[
                //'representante_id IS NULL'
            ],
            'contain' => ['Contactos']
        ]);
        $familiares=$this->Personas->find('list',[
            'conditions'=>[
                'representante_id'=>$persona->id
            ]
        ]);
        $this->set(compact('persona','familiares'));

    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $personas = $this->Personas->find('all',[
            'conditions'=>[
                //'representante_id IS NULL'
            ],
            'contain' => ['Estados', 'Municipios', 'Parroquias', 'Users']
        ]);

        $this->set(compact('personas'));
        $this->set('_serialize', ['personas']);
    }

    /**
     * View method
     *
     * @param string|null $id Persona id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if(!$id) $id=$this->Auth->user('id');
        $persona = $this->Personas->get($id, [
            'contain' => ['Estados', 'Municipios', 'Parroquias', 'Users', 'Contactos', 'Fotospersonas']
        ]);

        $this->set('persona', $persona);
        $this->set('_serialize', ['persona']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
     public function add($id=null)
     {
         $user=$this->Auth->user();
         $persona = $this->Personas->newEntity();
         if(!empty($id)){
             $persona->user_id=$id;
         }else{
             $persona->user_id=$user->id;
         }
         if ($this->request->is('post')) {
             $persona = $this->Personas->patchEntity($persona, $this->request->data);
             //debug($persona);
             $persona->nombre=strtoupper($persona->nombre);
             $persona->nombre_1=$persona->nombre_1?strtoupper($persona->nombre_1):$persona->nombre_1;
             $persona->apellido=strtoupper($persona->apellido);
             $persona->apellido_1=$persona->apellido_1?strtoupper($persona->apellido_1):$persona->apellido_1;
             $persona->bandera=1;
             if ($this->Personas->save($persona)) {
                 if($persona->user_id!=$user->id){
                     $this->Flash->success(__('Has guardado tus datos personales.'));
                     return $this->redirect(['controller'=>'personas','action' => 'info',$persona->id]);
                 }else{
                     $this->Flash->success(__('Ok. Ahora vuelve a iniciar sesión.'));
                     return $this->redirect(['controller'=>'users','action' => 'logout']);
                 }
             } else {
                 $this->Flash->error(__('The persona could not be saved. Please, try again.'));
             }
         }
         $estados = $this->getEstados();
         $this->set(compact('persona', 'estados'));
         $this->set('_serialize', ['persona']);
     }

     public function familia($id=null)
     {
         $user=$this->Auth->user();
         $persona = $this->Personas->newEntity();
         $id=$id?$id:$user->persona->id;
         $persona->representante_id=$id;
         $titular=$this->Personas->get($id);
         if ($this->request->is('post')) {
             $persona = $this->Personas->patchEntity($persona, $this->request->data);
             $persona->nombre=strtoupper($persona->nombre);
             $persona->nombre_1=$persona->nombre_1?strtoupper($persona->nombre_1):$persona->nombre_1;
             $persona->apellido=strtoupper($persona->apellido);
             $persona->apellido_1=$persona->apellido_1?strtoupper($persona->apellido_1):$persona->apellido_1;
             //debug($persona);
             $persona->bandera=1;
             if ($this->Personas->save($persona)) {
                 $this->Flash->success(__('Has guardado tus datos personales.'));
                 return $this->redirect(['controller'=>'personas','action' => 'familia',$id]);
             } else {
                 $this->Flash->error(__('The persona could not be saved. Please, try again.'));
             }
         }
         $familiares=$this->Personas->find('all')
            ->where(['representante_id'=>$id]);
         $estados = $this->getEstados();
         $this->set(compact('persona', 'estados','familiares','titular'));
         $this->set('_serialize', ['persona']);
     }

    /**
     * Edit method
     *
     * @param string|null $id Persona id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $persona = $this->Personas->get($id, [
            'contain' => ['Estados','Municipios','Parroquias']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $persona = $this->Personas->patchEntity($persona, $this->request->data);
            $persona->nombre=strtoupper($persona->nombre);
            $persona->nombre_1=$persona->nombre_1?strtoupper($persona->nombre_1):$persona->nombre_1;
            $persona->apellido=strtoupper($persona->apellido);
            $persona->apellido_1=$persona->apellido_1?strtoupper($persona->apellido_1):$persona->apellido_1;
            $persona->bandera=1;
            if ($this->Personas->save($persona)) {
                if($persona->user_id!=$user->id){
                    $this->Flash->success(__('Has actualizado los datos personales.'));
                    return $this->redirect(['controller'=>'personas','action' => 'info',$persona->id]);
                }else{
                    $this->Flash->success(__('Ok. Ahora vuelve a iniciar sesión.'));
                    return $this->redirect(['controller'=>'users','action' => 'logout']);
                }
            } else {
                $this->Flash->error(__('The persona could not be saved. Please, try again.'));
            }
        }
        $estados = $this->getEstados();
        $this->set(compact('persona', 'estados'));
        $this->set('_serialize', ['persona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Persona id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $persona = $this->Personas->get($id);
        if ($this->Personas->delete($persona)) {
            $this->Flash->success(__('The persona has been deleted.'));
        } else {
            $this->Flash->error(__('The persona could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
