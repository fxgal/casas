<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
/**
 * Rols Controller
 *
 * @property \App\Model\Table\RolsTable $Rols
 */
class ReportesController extends AppController
{


    public function asistencia()
    {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Personas');
        $personas = $this->Personas->find('all',[
            'conditions'=>['representante_id IS NULL'],
            'order'=>'apellido'
        ]);
        //debug($personas->toArray());
        $this->set(compact('personas'));
        $this->response->type('application/pdf');
    }
}
