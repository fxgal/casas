<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fotospersonas Controller
 *
 * @property \App\Model\Table\FotospersonasTable $Fotospersonas
 */
class FotospersonasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Personas']
        ];
        $fotospersonas = $this->paginate($this->Fotospersonas);

        $this->set(compact('fotospersonas'));
        $this->set('_serialize', ['fotospersonas']);
    }

    /**
     * View method
     *
     * @param string|null $id Fotospersona id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fotospersona = $this->Fotospersonas->get($id, [
            'contain' => ['Personas']
        ]);

        $this->set('fotospersona', $fotospersona);
        $this->set('_serialize', ['fotospersona']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fotospersona = $this->Fotospersonas->newEntity();
        if ($this->request->is('post')) {
            $fotospersona = $this->Fotospersonas->patchEntity($fotospersona, $this->request->data);
            if ($this->Fotospersonas->save($fotospersona)) {
                $this->Flash->success(__('The fotospersona has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fotospersona could not be saved. Please, try again.'));
            }
        }
        $personas = $this->Fotospersonas->Personas->find('list', ['limit' => 200]);
        $this->set(compact('fotospersona', 'personas'));
        $this->set('_serialize', ['fotospersona']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fotospersona id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fotospersona = $this->Fotospersonas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fotospersona = $this->Fotospersonas->patchEntity($fotospersona, $this->request->data);
            if ($this->Fotospersonas->save($fotospersona)) {
                $this->Flash->success(__('The fotospersona has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fotospersona could not be saved. Please, try again.'));
            }
        }
        $personas = $this->Fotospersonas->Personas->find('list', ['limit' => 200]);
        $this->set(compact('fotospersona', 'personas'));
        $this->set('_serialize', ['fotospersona']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fotospersona id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fotospersona = $this->Fotospersonas->get($id);
        if ($this->Fotospersonas->delete($fotospersona)) {
            $this->Flash->success(__('The fotospersona has been deleted.'));
        } else {
            $this->Flash->error(__('The fotospersona could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
