<?php $loguser = $this->request->session()->read('Auth.User'); ?>
<div class="col-md-10 col-md-offset-1 col-sm-12">
    <?= $this->Form->create($contacto) ?>
    <fieldset>
        <legend><?= $loguser->persona->nombre_simple.__(': Agregando datos de contacto') ?></legend>
        <div class="col-md-6 col-sm-12">
            <?php
                //echo $this->Form->hidden('persona_id');
                echo $this->Form->input('movil',['class'=>'form-control','label'=>'Telf. Móvil','placeholder'=>'04243334567']);
                echo $this->Form->input('casa',['class'=>'form-control','label'=>'Telf. Fijo','placeholder'=>'02468767878']);
            ?>
        </div>
        <div class="col-md-6 col-sm-12">
            <?php
                echo $this->Form->input('twitter',['class'=>'form-control','label'=>'Twitter','placeholder'=>'@usuario']);
                echo $this->Form->input('facebook',['class'=>'form-control','label'=>'Facebook']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
