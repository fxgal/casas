<?php $loguser = $this->request->session()->read('Auth.User'); ?>
<div class="col-md-10 col-md-offset-1 col-sm-12 fondo_blanco">
    <h3><?= __('Contactos') ?>
        <?php
        if($contactos->count()==0){
            echo $this->Html->link('Agregar',['controller' => 'contactos','action' => 'add',$loguser->persona->id],['class'=>'btn btn-success']);
        }else{
            echo $this->Html->link('Actualizar',['controller' => 'contactos','action' => 'edit',$contactos->toArray()[0]['id']],['class'=>'btn btn-warning']);
        }
        ?>
    </h3>
    <table cellpadding="0" cellspacing="0" class="table table-condensed table-striped table-responsive">
        <tbody>
            <?php foreach ($contactos as $contacto): ?>
            <tr>
                <th>
                    Telf. Móvil
                </th>
                <td><?= h($contacto->movil) ?></td>
                <tr>
                    <th>
                        Telf. Local
                    </th>
                    <td><?= h($contacto->casa) ?></td>
                </tr>
                <tr>
                    <th>
                        Twitter
                    </th>
                    <td>
                        <?php if($contacto->twitter){ ?>
                        <a href="https://twitter.com/<?= substr($contacto->twitter,1,1000) ?>" target="_blank"><?= h($contacto->twitter) ?></a>
                        <?php }else{ echo "No proporcionó.";} ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Facebook
                    </th>
                    <td>
                        <?php if($contacto->twitter){ ?>
                        <a href="https://www.facebook.com/<?= $contacto->facebook ?>" target="_blank"><?= h($contacto->facebook) ?></a>
                        <?php }else{ echo "No proporcionó.";} ?>
                    </td>
                </tr>
                <tr>
                    <th>
                        Correo Electrónico
                    </th>
                    <td><?= h($loguser->email) ?></td>
                </tr>
                <tr>
                    <td class="actions" colspan="2">
                        <?= $this->element('actions',['dato'=>$contacto])?>
                    </td>
                </tr>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
