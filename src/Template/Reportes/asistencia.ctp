<?php
require_once WWW_ROOT.DS. 'libs' .DS. 'tcpdf' .DS. 'Ficha.php';
setlocale(LC_TIME,"es_ES.UTF-8");
$pdf = new Ficha('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//establecer margenes
$pdf->SetMargins(10, 50, 10, 10);
$pdf->SetHeaderMargin(5);

//Salto de pagina automatico
$pdf->SetAutoPageBreak(true, 35);
$pdf->AddPage();
//primera hoja al pdf
$pdf->Ln(5);
$pdf->SetFont ('helvetica', 'B', 12);
$pdf->Cell(0, 7, 'LISTA DE ASISTENCIA', '0', 0, 'C', 0);
$pdf->Ln();
$pdf->Cell(20, 5, 'NOTAS:');
$pdf->TextField('notas', 170, 18, array('multiline'=>true, 'lineWidth'=>1, 'borderStyle'=>'solid'));
$pdf->Ln(19);
$pdf->Cell(0, 7, '', 'T', 0, 'L', 0);
$pdf->Ln();
//Linea 1
$pdf->SetFont ('helvetica', 'B', 12);
$pdf->Cell(15, 5, 'Nro. ', 1, 0, 'C', 0);
$pdf->Cell(55, 5, htmlspecialchars('APELLIDOS'), 1, 0, 'C', 0);
$pdf->Cell(55, 5, htmlspecialchars('NOMBRES'), 1, 0, 'C', 0);
$pdf->Cell(30, 5, htmlspecialchars('CÉDULA'), 1, 0, 'C', 0);
$pdf->Cell(35, 5, 'FIRMA', 1, 0, 'C', 0);
$pdf->Ln();
$pdf->SetFont ('helvetica', '', 12);
    //($file, $x=“, $y=”, $w=0, $h=0, $type=“, $link=”, $align=“, $resize=false, $dpi=300, $palign=”, $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array())
    //$pdf->Image(WWW_ROOT . $persona->foto, 162, 20, 25, 25,'','','',false,300,'',false,false,1);
    //$pdf->SetFont ('helvetica', '', 12);
    //debug($persona);
    //Cada linea tiene 156 espacios
$i=1;
foreach ($personas as $persona) {
    $pdf->Cell(15, 10, $i++, 1, 0, 'C', 0);
    $pdf->Cell(55, 10, $persona->apellido.' '.($persona->apellido_1?$persona->apellido_1:''), 1, 0, 'L', 0);
    $pdf->Cell(55, 10, $persona->nombre.' '.($persona->nombre_1?$persona->nombre_1:''), 1, 0, 'L', 0);
    $pdf->Cell(30, 10, $persona->cedula, 1, 0, 'L', 0);
    $pdf->Cell(35, 10, '', 1, 0, 'L', 0);
    $pdf->Ln();
}
$pdf->Ln(10);
$pdf->SetFont ('helvetica', '', 8);

$pdf->Cell(0, 7, "En la ciudad de San Juan de Los Morros, ".strftime('a los %d días del mes de %B del año %Y.'), 0, 0, 'L', 0);

$pdf->Ln(20);
//Cerramos el pdf
$pdf->lastPage ();

//Fin
$pdf->Output("Lista de Asistencia - ".date('d-m-Y').".pdf", 'D');
 ?>
