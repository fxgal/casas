<div class="col-md-55">
    <div class="thumbnail">
      <div class="image view view-first">
          <?= $this->Html->Image('/'.$foto->urlmin,['alt'=>$foto->id,'style'=>'width: 100%; display: block;']); ?>
        <div class="mask">
          <?php
          if(!$foto->active){
              ?><p>Disponible</p>
          <?php }else{
              ?><p>Activa</p><?php
          } ?>
          <div class="tools tools-bottom btn-group btn-group-xs">
              <?php if(!$foto->active){ ?>
                   <?= $this->Html->link('<i class="fa fa-check"></i>', [
                       'controller' => $controller,
                       'action' => 'active', $padre_id, $foto->id, $foto_actual_id],
                       ['class'=>'btn btn-primary btn-xs','escape'=>false]) ?>
                   <?= $this->Form->postLink('<i class="fa fa-trash-o"></i>', [
                       'controller' => $controller,
                       'action' => 'delete', $foto->id],
                       ['class'=>'btn btn-xs btn-danger','escape'=>false,'confirm' => __('No se recomienda eliminar fotos. Confirme # {0}?', $foto->id)]) ?>
              <?php } ?>
          </div>
        </div>
      </div>
      <div class="caption">
        <p>Última activación: <?= $this->Fechas->getFechaFormat($foto->modified,'dd/MM/y') ?></p>
      </div>
    </div>
</div>
