<div class="navbar nav_title" style="border: 0;">
  <a href="index.html" class="site_title"><i class="fa fa-circle"></i> <span>ASOBEA</span></a>
</div>
<div class="clearfix"></div>
<?php if(empty($persona)){?>
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>Inicie sesión</h3>
        </div>
    </div>
<?php }else{ ?>
    <!-- menu profile quick info -->
    <div class="profile">
      <div class="profile_pic">
          <?php
            if(!empty($persona->fotospersonas[0])){
                echo $this->Html->Image('/'.$persona->fotospersonas[0]['urlmin'],['alt'=>$persona->nombre,'class'=>'img-circle profile_img']);
            }else{
                echo $this->Html->Image('user.png',['alt'=>'Elija una foto','class'=>'img-circle profile_img']);
            }
          ?>
      </div>
      <div class="profile_info">
        <span>Bienvenido,</span>
        <h2><?= $persona->nombre_completo1 ?></h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-edit"></i> Datos Personales <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><?= $this->Html->link(__('Agregar'),['controller' => 'personas','action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Todos'),['controller' => 'personas','action' => 'index']); ?></li>
            <li><a>Por Rol<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <?= $this->cell('Rols::personasRol')?>
                </ul>
            </li>
        </ul>
      </li>
      <li><a><i class="fa fa-desktop"></i> Competencias <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><?= $this->Html->link(__('Agregar'),['controller' => 'competencias','action' => 'add']); ?></li>
          <li><?= $this->Html->link(__('Todas'),['controller' => 'competencias','action' => 'index']); ?></li>
          <li><a>Por Categoría<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                  <?= $this->cell('Competencias::competenciasCategoria')?>
              </ul>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-fire"></i> Clubes y Equipos <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><?= $this->Html->link(__('Agregar Club'),['controller' => 'clubs','action' => 'add']); ?></li>
          <li><?= $this->Html->link(__('Agregar Equipo'),['controller' => 'equipos','action' => 'add']); ?></li>
          <li><?= $this->Html->link(__('Todos'),['controller' => 'clubs','action' => 'index']); ?></li>
        </ul>
      </li>
      <li><a><i class="fa fa-asterisk"></i> Asociaciones <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><?= $this->Html->link(__('Agregar'),['controller' => 'asociacions','action' => 'add']); ?></li>
          <li><?= $this->Html->link(__('Todas'),['controller' => 'asociacions','action' => 'index']); ?></li>
        </ul>
      </li>
      <li><a><i class="fa fa-table"></i> Rosters <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><?= $this->Html->link(__('Agregar'),['controller' => 'rosters','action' => 'add']); ?></li>
            <li><?= $this->Html->link(__('Todos'),['controller' => 'rosters','action' => 'index']); ?></li>
        </ul>
      </li>
      <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
          <li><a href="fixed_footer.html">Fixed Footer</a></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Administración</h3>
    <ul class="nav side-menu">
        <li><a><i class="fa fa-home"></i> Noticias <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><?= $this->Html->link(__('Index'),['controller' => 'noticias','action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Agregar'),['controller' => 'noticias','action' => 'add']); ?></li>
          </ul>
        </li>
        <li><a><i class="fa fa-users"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><?= $this->Html->link(__('Todos'),['controller' => 'users','action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Agregar'),['controller' => 'users','action' => 'add']); ?></li>
          </ul>
        </li>
      <li><a><i class="fa fa-bug"></i> Ambitos <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><?= $this->Html->link(__('Todos'),['controller' => 'ambitos','action' => 'index']); ?></li>
            <li><?= $this->Html->link(__('Agregar'),['controller' => 'ambitos','action' => 'add']); ?></li>
        </ul>
      </li>
      <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="page_403.html">403 Error</a></li>
          <li><a href="page_404.html">404 Error</a></li>
          <li><a href="page_500.html">500 Error</a></li>
          <li><a href="plain_page.html">Plain Page</a></li>
          <li><a href="login.html">Login Page</a></li>
          <li><a href="pricing_tables.html">Pricing Tables</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li><a href="#level1_1">Level One</a>
            <li><a>Level One<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="level2.html">Level Two</a>
                </li>
                <li><a href="#level2_1">Level Two</a>
                </li>
                <li><a href="#level2_2">Level Two</a>
                </li>
              </ul>
            </li>
            <li><a href="#level1_2">Level One</a>
            </li>
        </ul>
      </li>
      <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
    </ul>
  </div>
</div>
<?php } ?>
