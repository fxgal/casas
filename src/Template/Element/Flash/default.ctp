<?php
$class = 'message col-md-6 col-md-offset-3 col-sm-12';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
?>
<div class="<?= h($class) ?>"><?= h($message) ?></div>
