<div class="paginator col-md-4 col-md-offset-4">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
    </ul>
    <p>Page <?= $this->Paginator->counter() ?></p>
</div>
