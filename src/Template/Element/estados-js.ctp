<?= $this->Html->scriptStart(['block'=>true]); ?>
$('#estado-id').on('change', function() {
  municipios($(this).val());
});
function municipios(estado){
    $.ajax({
      url: '<?= $this->Url->build(['controller'=>'municipios','action'=>'lista'])?>/'+estado,
      type: 'GET',
      beforeSend: function (xhr) {
        $("#municipio-id").html("<option>Cargando...</option>");
      },
      success: function (data, textStatus, jqXHR) {
        $("#municipio-id").replaceWith(data);
        console.log(textStatus);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
      },
      complete: function (jqXHR, textStatus) {
        //$('#municipio-id').trigger('change');
      }
    });
}
//$('#estado-id').trigger('change');
<?= $this->Html->scriptEnd(); ?>
