<script type="text/javascript">
<?= $this->Html->scriptStart(['block'=>'datatables']); ?>
$("#<?= $table_id ?>").DataTable({
    language: {
    processing:     "Buscando...",
    search:         "Buscar:",
    lengthMenu:    "Mostrar _MENU_ elementos",
    info:           "Mostrando del _START_ al _END_ de _TOTAL_ elementos",
    infoEmpty:      "Mostrando del 0 al 0 de 0 elementos",
    //infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    infoPostFix:    "",
    //loadingRecords: "Chargement en cours...",
    zeroRecords:    "Datos no encontrados",
    emptyTable:     "No hay datos disponibles",
    paginate: {
        first:      "Primero",
        previous:   "Anterior",
        next:       "Siguiente",
        last:       "Último"
    },
    aria: {
        sortAscending:  ": organizar en orden ascendente",
        sortDescending: ": organizar en orden descendente"
    }
}
});
<?= $this->Html->scriptEnd(); ?>
</script>
