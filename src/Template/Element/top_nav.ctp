

<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#" id="">
        <?= $this->Html->image('casa.png', ['alt' => 'Logo','id'=>'logotipo']); ?>
    </a>
  </div>

  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
        <li>
            <?= $this->Html->link(__('<i class="fa fa-home"></i> Principal'),
                    [
                        'controller' => 'personas',
                        'action' => 'info',
                    ],
                    ['escape'=>false]
                );?>
        </li>
        <li>
            <?= $this->Html->link(__('<i class="fa fa-users"></i> Usuarios'),
                    [
                        'controller' => 'users',
                        'action' => 'index',
                    ],
                    ['escape'=>false]
                );?>
        </li>
      <li>
          <?= $this->Html->link(__('<i class="fa fa-inbox"></i> Personas'),
                  [
                      'controller' => 'personas',
                      'action' => 'index',
                  ],
                  ['escape'=>false]
              );?>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-print"></i> Reportes <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li>
              <?= $this->Html->link(__('<i class="fa fa-list"></i> Asistencia'),
                      [
                          'controller' => 'reportes',
                          'action' => 'asistencia',
                      ],
                      ['escape'=>false]
                  );?>
          </li>
          <li><a href="#">Agenda de contactos</a></li>
          <li><a href="#">Carga Familiar</a></li>
          <li><a href="#">Requerimientos</a></li>
          <li class="divider"></li>
          <li><a href="#">Lista por Genero</a></li>
        </ul>
      </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        <?php $usuario=$loguser->persona->nombre_simple?$loguser->persona->nombre_simple:$loguser['email']; ?>
      <li><a><?=$usuario.'. '.$this->Fechas->getFechaFormat($loguser->access,'dd-MM-yyyy hh:mm:ss a')?></a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
         <i class="fa fa-user"></i> <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li>
              <?= $this->Html->link(__('<i class="fa fa-puzzle-piece"></i> Perfil'),
                      [
                          'controller' => 'personas',
                          'action' => 'view',
                          $loguser->persona->id
                      ],
                      ['escape'=>false]
                  );?>
          </li>
          <li class="divider"></li>
          <li>
              <?= $this->Html->link(__('<i class="fa fa-trash"></i> Salir'),
                      [
                          'controller' => 'users',
                          'action' => 'logout',
                      ],
                      ['escape'=>false]
                  );?>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
