<div class="btn btn-group">
    <?= $this->Html->link('<i class="fa fa-eye"></i>', ['action' => 'view', $dato->id],['class'=>'btn btn-xs btn-primary','escape'=>false]) ?>
    <?= $this->Html->link('<i class="fa fa-edit"></i>', ['action' => 'edit', $dato->id],['class'=>'btn btn-xs btn-warning','escape'=>false]) ?>
    <?= $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'delete', $dato->id], ['escape'=>false,'class'=>'btn btn-xs btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $dato->id)]) ?>
</div>
