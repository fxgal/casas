<div class="col-md-6">
<span class="btn btn-default btn-file" id="buscar_foto">
    Buscar <i class="fa fa-picture-o"></i>
<?php
    echo $this->Form->input('foto',[
  'type'=>'file',
  'id'=>'foto',
  'accept'=>'image/jpeg',
  'label'=>false
  ]);
?>
</span>
</div>
<div class="col-md-6">
    <button id="eliminar_foto" style="display: none">Eliminar <i class="fa fa-trash-o"></i></button>
</div>
<div class="col-md-12 col-sm-12" id="imagen" style="display: none">
  <img src="" id="imgSalida" class="img-responsive img-thumbnail img-rounded"/>
  <input type="hidden" id="x" name="x" />
  <input type="hidden" id="y" name="y" />
  <input type="hidden" id="w" name="w" />
  <input type="hidden" id="h" name="h" />
</div>
