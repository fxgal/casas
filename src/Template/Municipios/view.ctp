<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Municipio'), ['action' => 'edit', $municipio->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Municipio'), ['action' => 'delete', $municipio->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipio->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['controller' => 'Estados', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['controller' => 'Estados', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Clubs'), ['controller' => 'Clubs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Club'), ['controller' => 'Clubs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parroquias'), ['controller' => 'Parroquias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parroquia'), ['controller' => 'Parroquias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Residencia'), ['controller' => 'Residencia', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Residencium'), ['controller' => 'Residencia', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="municipios view large-9 medium-8 columns content">
    <h3><?= h($municipio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= $municipio->has('estado') ? $this->Html->link($municipio->estado->id, ['controller' => 'Estados', 'action' => 'view', $municipio->estado->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= h($municipio->municipio) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($municipio->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Clubs') ?></h4>
        <?php if (!empty($municipio->clubs)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Abreviacion') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Asociacion Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($municipio->clubs as $clubs): ?>
            <tr>
                <td><?= h($clubs->id) ?></td>
                <td><?= h($clubs->nombre) ?></td>
                <td><?= h($clubs->abreviacion) ?></td>
                <td><?= h($clubs->municipio_id) ?></td>
                <td><?= h($clubs->asociacion_id) ?></td>
                <td><?= h($clubs->created) ?></td>
                <td><?= h($clubs->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Clubs', 'action' => 'view', $clubs->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Clubs', 'action' => 'edit', $clubs->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Clubs', 'action' => 'delete', $clubs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $clubs->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Parroquias') ?></h4>
        <?php if (!empty($municipio->parroquias)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Parroquia') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($municipio->parroquias as $parroquias): ?>
            <tr>
                <td><?= h($parroquias->id) ?></td>
                <td><?= h($parroquias->municipio_id) ?></td>
                <td><?= h($parroquias->parroquia) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Parroquias', 'action' => 'view', $parroquias->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Parroquias', 'action' => 'edit', $parroquias->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Parroquias', 'action' => 'delete', $parroquias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $parroquias->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Personas') ?></h4>
        <?php if (!empty($municipio->personas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nacionalidad') ?></th>
                <th><?= __('Cedula') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Nombre 1') ?></th>
                <th><?= __('Apellido') ?></th>
                <th><?= __('Apellido 1') ?></th>
                <th><?= __('Nacimiento') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Parroquia Id') ?></th>
                <th><?= __('Contacto Id') ?></th>
                <th><?= __('Usuario Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($municipio->personas as $personas): ?>
            <tr>
                <td><?= h($personas->id) ?></td>
                <td><?= h($personas->nacionalidad) ?></td>
                <td><?= h($personas->cedula) ?></td>
                <td><?= h($personas->nombre) ?></td>
                <td><?= h($personas->nombre_1) ?></td>
                <td><?= h($personas->apellido) ?></td>
                <td><?= h($personas->apellido_1) ?></td>
                <td><?= h($personas->nacimiento) ?></td>
                <td><?= h($personas->estado_id) ?></td>
                <td><?= h($personas->municipio_id) ?></td>
                <td><?= h($personas->parroquia_id) ?></td>
                <td><?= h($personas->contacto_id) ?></td>
                <td><?= h($personas->usuario_id) ?></td>
                <td><?= h($personas->created) ?></td>
                <td><?= h($personas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Personas', 'action' => 'view', $personas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Personas', 'action' => 'edit', $personas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Personas', 'action' => 'delete', $personas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $personas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Residencia') ?></h4>
        <?php if (!empty($municipio->residencia)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Parroquia Id') ?></th>
                <th><?= __('Ficha Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($municipio->residencia as $residencia): ?>
            <tr>
                <td><?= h($residencia->id) ?></td>
                <td><?= h($residencia->estado_id) ?></td>
                <td><?= h($residencia->municipio_id) ?></td>
                <td><?= h($residencia->parroquia_id) ?></td>
                <td><?= h($residencia->ficha_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Residencia', 'action' => 'view', $residencia->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Residencia', 'action' => 'edit', $residencia->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Residencia', 'action' => 'delete', $residencia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $residencia->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
