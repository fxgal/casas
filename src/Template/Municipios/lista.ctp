<?php

echo $this->Form->input('municipio_id', ['options' => $municipios,'label'=>false,'div'=>false,'class'=>'form-control']);

 ?>
<script type="text/javascript">
$('#municipio-id').on('change', function() {
  municipio=($(this).val());
  $.ajax({
    url: '<?= $this->Url->build(['controller'=>'parroquias','action'=>'lista'])?>/'+municipio,
    type: 'GET',
    beforeSend: function (xhr) {
      $("#parroquia-id").html("<option>Cargando...</option>");
    },
    success: function (data, textStatus, jqXHR) {
      $("#parroquia-id").replaceWith(data);
      console.log(textStatus);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(errorThrown);
    },
    complete: function (jqXHR, textStatus) {
      //$('#municipio-id').trigger('change');
    }
  });
});
$('#municipio-id').trigger('change');
</script>
