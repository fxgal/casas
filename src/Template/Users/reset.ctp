<div class="col-md-4 col-md-offset-4 col-sm-12">
    <div class="" id="mensaje-reset">

    </div>
    <?= $this->Form->create($user,['id'=>'user-edit']) ?>
    <fieldset>
        <legend><?= __('Recuperar Clave') ?></legend>
        <?php
            echo $this->Form->input('email',['class'=>'form-control','placeholder'=>'usuario@dominio.com']);
            echo $this->Form->input('password',['class'=>'form-control','pattern'=>".{6,10}",'label'=>'Clave']);
            echo $this->Form->input('password2',['class'=>'form-control','type'=>'password','label'=>'Repetir Clave','pattern'=>".{6,10}"]);
        ?>
    </fieldset>
    <div class="btn-group">
        <button type="button" class="btn btn-primary" id="user-submit">Actualizar</button>
        <button type="button" class="btn btn-danger" onclick="history.back()">Volver</button>
    </div>
</div>
<script type="text/javascript">
<?= $this->Html->scriptStart(['block'=>true]); ?>
$('#user-submit').on('click', function(event) {
    event.preventDefault();
    if(!validar()){
        return false;
    }else{
        $("#user-edit").trigger('submit');
    }
});
$("#password").val("");
function validar(){
    if($("#password").val().length<6 || $("#password").val()!=$("#password2").val() || $("#email").val().length<10){
        $("#password").attr({
            class: 'input_error form-control',
        });
        $("#password2").attr({
            class: 'input_error form-control',
        });
        $("#password").val("");
        $("#password2").val("");
        $("#mensaje-reset").attr({
            onclick:"this.classList.add('hidden')",
            class:"alert alert-dismissible alert-danger col-md-6 col-md-offset-3 col-sm-12"
        });
        $("#mensaje-reset").html("Datos incorrectos.");
        return false;
    }else{
        $("#password").attr({
            class: 'input_success form-control',
        });
        $("#password2").attr({
            class: 'input_success form-control',
        });
        return true;
    }
}
<?= $this->Html->scriptEnd(); ?>
</script>
