<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user,['id'=>'user-edit']) ?>
    <fieldset>
        <legend><?= __('Editar Usuario') ?></legend>
        <?php
            echo $this->Form->input('email',['class'=>'form-control']);
            echo $this->Form->input('password',['class'=>'form-control','pattern'=>".{6,10}"]);
            echo $this->Form->input('password2',['class'=>'form-control','type'=>'password','label'=>'Repetir Password','pattern'=>".{6,10}"]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
<?= $this->Html->scriptStart(['block'=>true]); ?>
$("#password").val("");
$("#user-edit").on('submit', function(event) {
    if(!validar()){
        return false;
    }
});
function validar(){
    if($("#password").val()!=$("#password2").val()){
        $("#password").attr({
            class: 'input_error form-control',
        });
        $("#password2").attr({
            class: 'input_error form-control',
        });
        return false;
    }else{
        $("#password").attr({
            class: 'input_success form-control',
        });
        $("#password2").attr({
            class: 'input_success form-control',
        });
        return true;
    }
}
<?= $this->Html->scriptEnd(); ?>
</script>
