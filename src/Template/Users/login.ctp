<div class="col-md-6 col-sm-10 col-md-offset-3 col-sm-offset-1">
  <section class="login_content">
      <?= $this->Form->create() ?>
      <h1>Control de Acceso</h1>
      <div>
        <?= $this->Form->input('email',['class'=>'form-control','placeholder'=>'Email','label'=>false]) ?>
      </div>
      <div>
        <?= $this->Form->input('password',['class'=>'form-control','placeholder'=>'Clave de Acceso','label'=>false]) ?>
      </div>
      <div class=" col-sm-6 col-sm-offset-3">
        <?= $this->Form->button(__('Entrar'),['class'=>'btn btn-primary']); ?>
        <?php echo $this->Html->link(__('¿Olvidaste tu clave?'),
                [
                    'controller' => 'users',
                    'action' => 'reset'
                ]
            ); ?>
      </div>
    <?= $this->Form->end() ?>
  </section>
</div>
</div>
