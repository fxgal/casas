<div class="users view large-9 medium-8 columns content fondo_blanco">
    <h3><?= h($user->email) ?></h3>
    <table class="vertical-table table table-condensed table-striped">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Access') ?></th>
            <td><?= h($user->access) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Datos Personales') ?></h4>
        <?php if (!empty($user->personas)){ ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nacionalidad') ?></th>
                <th scope="col"><?= __('Cedula') ?></th>
                <th scope="col"><?= __('Nombre') ?></th>
                <th scope="col"><?= __('Nombre 1') ?></th>
                <th scope="col"><?= __('Apellido') ?></th>
                <th scope="col"><?= __('Apellido 1') ?></th>
                <th scope="col"><?= __('Sexo') ?></th>
                <th scope="col"><?= __('Nacimiento') ?></th>
                <th scope="col"><?= __('Estado Id') ?></th>
                <th scope="col"><?= __('Municipio Id') ?></th>
                <th scope="col"><?= __('Parroquia Id') ?></th>
                <th scope="col"><?= __('Contacto Id') ?></th>
                <th scope="col"><?= __('Representante Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->personas as $personas): ?>
            <tr>
                <td><?= h($personas->id) ?></td>
                <td><?= h($personas->nacionalidad) ?></td>
                <td><?= h($personas->cedula) ?></td>
                <td><?= h($personas->nombre) ?></td>
                <td><?= h($personas->nombre_1) ?></td>
                <td><?= h($personas->apellido) ?></td>
                <td><?= h($personas->apellido_1) ?></td>
                <td><?= h($personas->sexo) ?></td>
                <td><?= h($personas->nacimiento) ?></td>
                <td><?= h($personas->estado_id) ?></td>
                <td><?= h($personas->municipio_id) ?></td>
                <td><?= h($personas->parroquia_id) ?></td>
                <td><?= h($personas->contacto_id) ?></td>
                <td><?= h($personas->representante_id) ?></td>
                <td><?= h($personas->user_id) ?></td>
                <td><?= h($personas->created) ?></td>
                <td><?= h($personas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Personas', 'action' => 'view', $personas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Personas', 'action' => 'edit', $personas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Personas', 'action' => 'delete', $personas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $personas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php }else{ ?>
            <?= $this->Html->link(__('Agregar'), ['controller' => 'Personas', 'action' => 'add', $user->id],['class'=>'btn btn-primary']) ?>
        <?php } ?>
    </div>
</div>
