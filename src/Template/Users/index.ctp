
<div class="col-md-10 col-md-offset-1 fondo_blanco">
    <h3><?= __('Usuarios') ?></h3>
    <table cellpadding="0" cellspacing="0" id="users_index" class="table table-condensed table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th scope="col">Acceso</th>
                <th scope="col">Registro</th>
                <th scope="col">Actualización</th>
                <th scope="col" class="actions">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= $user->rol->nombre; ?></td>
                <td><?= h($user->access) ?></td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->modified) ?></td>
                <td class="actions">
                    <?= $this->element('actions',['dato'=>$user])?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
<?= $this->Html->scriptStart(['block'=>true]); ?>
    $('#users_index').DataTable({
        language: {
        processing:     "Buscando...",
        search:         "Buscar:",
        lengthMenu:    "Mostrar _MENU_ elementos",
        info:           "Mostrando del _START_ al _END_ de _TOTAL_ elementos",
        infoEmpty:      "Mostrando del 0 al 0 de 0 elementos",
        //infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        infoPostFix:    "",
        //loadingRecords: "Chargement en cours...",
        zeroRecords:    "Datos no encontrados",
        emptyTable:     "No hay datos disponibles",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Último"
        },
        aria: {
            sortAscending:  ": organizar en orden ascendente",
            sortDescending: ": organizar en orden descendente"
        }
    }
    });
<?= $this->Html->scriptEnd(); ?>
</script>
