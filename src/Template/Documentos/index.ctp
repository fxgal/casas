<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Documento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="documentos index large-9 medium-8 columns content">
    <h3><?= __('Documentos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('requerido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('abreviacion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($documentos as $documento): ?>
            <tr>
                <td><?= $this->Number->format($documento->id) ?></td>
                <td><?= h($documento->nombre) ?></td>
                <td><?= h($documento->requerido) ?></td>
                <td><?= h($documento->abreviacion) ?></td>
                <td><?= h($documento->created) ?></td>
                <td><?= h($documento->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $documento->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $documento->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $documento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
