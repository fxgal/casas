<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $documento->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $documento->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Documentos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="documentos form large-9 medium-8 columns content">
    <?= $this->Form->create($documento) ?>
    <fieldset>
        <legend><?= __('Edit Documento') ?></legend>
        <?php
            echo $this->Form->input('nombre');
            echo $this->Form->input('requerido');
            echo $this->Form->input('abreviacion');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
