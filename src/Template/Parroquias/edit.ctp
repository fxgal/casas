<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $parroquia->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $parroquia->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Parroquias'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Residencia'), ['controller' => 'Residencia', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Residencium'), ['controller' => 'Residencia', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="parroquias form large-9 medium-8 columns content">
    <?= $this->Form->create($parroquia) ?>
    <fieldset>
        <legend><?= __('Edit Parroquia') ?></legend>
        <?php
            echo $this->Form->input('municipio_id', ['options' => $municipios]);
            echo $this->Form->input('parroquia');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
