<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Parroquia'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Residencia'), ['controller' => 'Residencia', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Residencium'), ['controller' => 'Residencia', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="parroquias index large-9 medium-8 columns content">
    <h3><?= __('Parroquias') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('municipio_id') ?></th>
                <th><?= $this->Paginator->sort('parroquia') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($parroquias as $parroquia): ?>
            <tr>
                <td><?= $this->Number->format($parroquia->id) ?></td>
                <td><?= $parroquia->has('municipio') ? $this->Html->link($parroquia->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $parroquia->municipio->id]) : '' ?></td>
                <td><?= h($parroquia->parroquia) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $parroquia->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $parroquia->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $parroquia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $parroquia->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
