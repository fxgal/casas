<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Parroquia'), ['action' => 'edit', $parroquia->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Parroquia'), ['action' => 'delete', $parroquia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $parroquia->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Parroquias'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parroquia'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Residencia'), ['controller' => 'Residencia', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Residencium'), ['controller' => 'Residencia', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="parroquias view large-9 medium-8 columns content">
    <h3><?= h($parroquia->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Municipio') ?></th>
            <td><?= $parroquia->has('municipio') ? $this->Html->link($parroquia->municipio->id, ['controller' => 'Municipios', 'action' => 'view', $parroquia->municipio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Parroquia') ?></th>
            <td><?= h($parroquia->parroquia) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($parroquia->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Personas') ?></h4>
        <?php if (!empty($parroquia->personas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nacionalidad') ?></th>
                <th><?= __('Cedula') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Nombre 1') ?></th>
                <th><?= __('Apellido') ?></th>
                <th><?= __('Apellido 1') ?></th>
                <th><?= __('Nacimiento') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Parroquia Id') ?></th>
                <th><?= __('Contacto Id') ?></th>
                <th><?= __('Usuario Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($parroquia->personas as $personas): ?>
            <tr>
                <td><?= h($personas->id) ?></td>
                <td><?= h($personas->nacionalidad) ?></td>
                <td><?= h($personas->cedula) ?></td>
                <td><?= h($personas->nombre) ?></td>
                <td><?= h($personas->nombre_1) ?></td>
                <td><?= h($personas->apellido) ?></td>
                <td><?= h($personas->apellido_1) ?></td>
                <td><?= h($personas->nacimiento) ?></td>
                <td><?= h($personas->estado_id) ?></td>
                <td><?= h($personas->municipio_id) ?></td>
                <td><?= h($personas->parroquia_id) ?></td>
                <td><?= h($personas->contacto_id) ?></td>
                <td><?= h($personas->usuario_id) ?></td>
                <td><?= h($personas->created) ?></td>
                <td><?= h($personas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Personas', 'action' => 'view', $personas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Personas', 'action' => 'edit', $personas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Personas', 'action' => 'delete', $personas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $personas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Residencia') ?></h4>
        <?php if (!empty($parroquia->residencia)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Parroquia Id') ?></th>
                <th><?= __('Ficha Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($parroquia->residencia as $residencia): ?>
            <tr>
                <td><?= h($residencia->id) ?></td>
                <td><?= h($residencia->estado_id) ?></td>
                <td><?= h($residencia->municipio_id) ?></td>
                <td><?= h($residencia->parroquia_id) ?></td>
                <td><?= h($residencia->ficha_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Residencia', 'action' => 'view', $residencia->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Residencia', 'action' => 'edit', $residencia->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Residencia', 'action' => 'delete', $residencia->id], ['confirm' => __('Are you sure you want to delete # {0}?', $residencia->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
