<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Fotospersonas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fotospersonas form large-9 medium-8 columns content">
    <?= $this->Form->create($fotospersona) ?>
    <fieldset>
        <legend><?= __('Add Fotospersona') ?></legend>
        <?php
            echo $this->Form->input('persona_id', ['options' => $personas]);
            echo $this->Form->input('url');
            echo $this->Form->input('urlmin');
            echo $this->Form->input('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
