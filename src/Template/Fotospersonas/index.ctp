<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Fotospersona'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fotospersonas index large-9 medium-8 columns content">
    <h3><?= __('Fotospersonas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('persona_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('url') ?></th>
                <th scope="col"><?= $this->Paginator->sort('urlmin') ?></th>
                <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fotospersonas as $fotospersona): ?>
            <tr>
                <td><?= $this->Number->format($fotospersona->id) ?></td>
                <td><?= $fotospersona->has('persona') ? $this->Html->link($fotospersona->persona->id, ['controller' => 'Personas', 'action' => 'view', $fotospersona->persona->id]) : '' ?></td>
                <td><?= h($fotospersona->url) ?></td>
                <td><?= h($fotospersona->urlmin) ?></td>
                <td><?= h($fotospersona->active) ?></td>
                <td><?= h($fotospersona->created) ?></td>
                <td><?= h($fotospersona->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fotospersona->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fotospersona->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fotospersona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fotospersona->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
