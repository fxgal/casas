<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fotospersona'), ['action' => 'edit', $fotospersona->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fotospersona'), ['action' => 'delete', $fotospersona->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fotospersona->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fotospersonas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fotospersona'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fotospersonas view large-9 medium-8 columns content">
    <h3><?= h($fotospersona->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Persona') ?></th>
            <td><?= $fotospersona->has('persona') ? $this->Html->link($fotospersona->persona->id, ['controller' => 'Personas', 'action' => 'view', $fotospersona->persona->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= h($fotospersona->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Urlmin') ?></th>
            <td><?= h($fotospersona->urlmin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fotospersona->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fotospersona->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fotospersona->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $fotospersona->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
