<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Estado'), ['action' => 'edit', $estado->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Estado'), ['action' => 'delete', $estado->id], ['confirm' => __('Are you sure you want to delete # {0}?', $estado->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Estados'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Estado'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Asociacions'), ['controller' => 'Asociacions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Asociacion'), ['controller' => 'Asociacions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Competencias'), ['controller' => 'Competencias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Competencia'), ['controller' => 'Competencias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Municipios'), ['controller' => 'Municipios', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Municipio'), ['controller' => 'Municipios', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Personas'), ['controller' => 'Personas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Persona'), ['controller' => 'Personas', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="estados view large-9 medium-8 columns content">
    <h3><?= h($estado->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= h($estado->estado) ?></td>
        </tr>
        <tr>
            <th><?= __('Abreviacion') ?></th>
            <td><?= h($estado->abreviacion) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($estado->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Asociacions') ?></h4>
        <?php if (!empty($estado->asociacions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Abreviacion') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($estado->asociacions as $asociacions): ?>
            <tr>
                <td><?= h($asociacions->id) ?></td>
                <td><?= h($asociacions->nombre) ?></td>
                <td><?= h($asociacions->abreviacion) ?></td>
                <td><?= h($asociacions->estado_id) ?></td>
                <td><?= h($asociacions->created) ?></td>
                <td><?= h($asociacions->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Asociacions', 'action' => 'view', $asociacions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Asociacions', 'action' => 'edit', $asociacions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Asociacions', 'action' => 'delete', $asociacions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $asociacions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Competencias') ?></h4>
        <?php if (!empty($estado->competencias)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Logo') ?></th>
                <th><?= __('Eslogan') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Periodo') ?></th>
                <th><?= __('Sede') ?></th>
                <th><?= __('Modalidad') ?></th>
                <th><?= __('Estatus') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($estado->competencias as $competencias): ?>
            <tr>
                <td><?= h($competencias->id) ?></td>
                <td><?= h($competencias->nombre) ?></td>
                <td><?= h($competencias->logo) ?></td>
                <td><?= h($competencias->eslogan) ?></td>
                <td><?= h($competencias->estado_id) ?></td>
                <td><?= h($competencias->periodo) ?></td>
                <td><?= h($competencias->sede) ?></td>
                <td><?= h($competencias->modalidad) ?></td>
                <td><?= h($competencias->estatus) ?></td>
                <td><?= h($competencias->created) ?></td>
                <td><?= h($competencias->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Competencias', 'action' => 'view', $competencias->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Competencias', 'action' => 'edit', $competencias->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Competencias', 'action' => 'delete', $competencias->id], ['confirm' => __('Are you sure you want to delete # {0}?', $competencias->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Municipios') ?></h4>
        <?php if (!empty($estado->municipios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Municipio') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($estado->municipios as $municipios): ?>
            <tr>
                <td><?= h($municipios->id) ?></td>
                <td><?= h($municipios->estado_id) ?></td>
                <td><?= h($municipios->municipio) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Municipios', 'action' => 'view', $municipios->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Municipios', 'action' => 'edit', $municipios->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Municipios', 'action' => 'delete', $municipios->id], ['confirm' => __('Are you sure you want to delete # {0}?', $municipios->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Personas') ?></h4>
        <?php if (!empty($estado->personas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nacionalidad') ?></th>
                <th><?= __('Cedula') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Nombre 1') ?></th>
                <th><?= __('Apellido') ?></th>
                <th><?= __('Apellido 1') ?></th>
                <th><?= __('Sexo') ?></th>
                <th><?= __('Nacimiento') ?></th>
                <th><?= __('Estado Id') ?></th>
                <th><?= __('Municipio Id') ?></th>
                <th><?= __('Parroquia Id') ?></th>
                <th><?= __('Contacto Id') ?></th>
                <th><?= __('Usuario Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($estado->personas as $personas): ?>
            <tr>
                <td><?= h($personas->id) ?></td>
                <td><?= h($personas->nacionalidad) ?></td>
                <td><?= h($personas->cedula) ?></td>
                <td><?= h($personas->nombre) ?></td>
                <td><?= h($personas->nombre_1) ?></td>
                <td><?= h($personas->apellido) ?></td>
                <td><?= h($personas->apellido_1) ?></td>
                <td><?= h($personas->sexo) ?></td>
                <td><?= h($personas->nacimiento) ?></td>
                <td><?= h($personas->estado_id) ?></td>
                <td><?= h($personas->municipio_id) ?></td>
                <td><?= h($personas->parroquia_id) ?></td>
                <td><?= h($personas->contacto_id) ?></td>
                <td><?= h($personas->usuario_id) ?></td>
                <td><?= h($personas->created) ?></td>
                <td><?= h($personas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Personas', 'action' => 'view', $personas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Personas', 'action' => 'edit', $personas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Personas', 'action' => 'delete', $personas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $personas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
