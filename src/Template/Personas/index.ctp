<div class="col-md-10 col-md-offset-1 col-sm-12 fondo_blanco">
    <h3><?= __('Personas') ?></h3>
    <table cellpadding="0" cellspacing="0" class="table table-responsive table-condensed table-striped" id="personas_index">
        <thead>
            <tr>
                <th>Id</th>
                <th>Situación</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Sexo</th>
                <th>Edad</th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($personas as $persona): ?>
            <tr>
                <td><?= $this->Number->format($persona->id) ?></td>
                <td>
                    <span class="label <?= $persona->bandera==1?'label-success':'label-danger'; ?>">
                        <?= $persona->bandera==1?'Actualizado':'Pendiente'; ?>
                    </span>
                </td>
                <td><?= $persona->cedula>0?$this->Number->format($persona->cedula):'No tiene'; ?></td>
                <td><?= h($persona->nombre) ?></td>
                <td><?= h($persona->apellido) ?></td>
                <td><?= $persona->sexo==1?'Femenino':'Masculino'; ?></td>
                <td><?= $this->Fechas->getEdad($persona->nacimiento) ?></td>
                <td class="actions">
                    <?= $this->element('actions',['dato'=>$persona])?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= $this->element('datatable',['table_id'=>'personas_index']); ?>
