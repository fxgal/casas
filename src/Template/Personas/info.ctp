<?php $loguser = $this->request->session()->read('Auth.User');?>
<div class="col-md-10 col-xs-12 col-md-offset-1 fondo_blanco" id="panel_general">
    <h1>Panel de Información General</h1>
    <div class="col-md-2 col-sm-12">
        <?php if($loguser->persona->id){ ?>
        <span class="info_principal">
            <i class="fa fa-check"></i>
        </span>
        <?php } ?>
        <?php echo $this->Html->link('
        <i class="fa fa-5x fa-user"></i>
        <br>
        Datos Personales',
                [
                    'controller' => 'personas',
                    'action' => 'view',
                    $loguser->persona->id
                ],
                ['escape'=>false]
            ); ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <span class="info_principal">
            <?=$familiares->count(); ?>
        </span>
        <?php echo $this->Html->link('
        <i class="fa fa-5x fa-users"></i>
        <br>
        Carga Familiar',
                [
                    'controller' => 'personas',
                    'action' => 'familia',
                    $loguser->persona->id
                ],
                ['escape'=>false]
            ); ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <i class="fa fa-5x fa-home"></i>
        <br>
        Socioeconómicos
    </div>
    <div class="col-md-2 col-sm-12">
        <i class="fa fa-5x fa-clipboard"></i>
        <br>
        Datos Laborales
    </div>
    <div class="col-md-2 col-sm-12">

        <?php if(!empty($persona->contacto)){ ?>
            <span class="info_principal">
                <i class="fa fa-check"></i>
            </span>
        <?php } ?>
        <?php echo $this->Html->link('
        <i class="fa fa-5x fa-comments"></i>
        <br>
        Contacto',
                [
                    'controller' => 'contactos',
                    'action' => 'info'
                ],
                ['escape'=>false]
            ); ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <i class="fa fa-5x fa-inbox"></i>
        <br>
        Bandeja de Entrada
    </div>
    <div class="col-md-2 col-sm-12">
        <i class="fa fa-5x fa-facebook-square"></i>
        <br>
        Enlace a Facebook
    </div>
    <div class="col-md-2 col-sm-12">
        <i class="fa fa-5x fa-twitter-square"></i>
        <br>
        Enlace a Twitter
    </div>
</div>
