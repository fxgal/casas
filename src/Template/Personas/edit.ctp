<div class="col-md-10 col-xs-12 col-md-offset-1">
    <?= $this->Form->create($persona) ?>
    <fieldset>
        <legend><?= __('Actualizar datos personales') ?></legend>
        <div class="col-md-6 col-xs-12">
            <div class="col-md-6 col-sm-12">
                <?php echo $this->Form->input('nacionalidad',['class'=>'form-control','options'=>['Venezolano'=>'Venezolano','Extrangero'=>'Extrangero']]); ?>
            </div>
            <div class="col-md-6 col-sm-12">
                <?php echo $this->Form->hidden('id'); ?>
                <?php echo $this->Form->input('cedula',['class'=>'form-control']); ?>
            </div>
            <?php
                echo $this->Form->input('nombre',['class'=>'form-control']);
                echo $this->Form->input('nombre_1',['class'=>'form-control','label'=>'Segundo Nombre']);
                echo $this->Form->input('apellido',['class'=>'form-control']);
                echo $this->Form->input('apellido_1',['class'=>'form-control','label'=>'Segundo Apellido']);
            ?>
        </div>
        <div class="col-md-6 col-xs-12">
            <label>Sexo</label>
            <?php
                echo $this->Form->radio('sexo',['0'=>'Masculino','1'=>'Femenino']);
                echo $this->Form->input('nacimiento',[
                    'class'=>'form-control',
                    'label'=>'Fecha de Nacimiento',
                    'minYear'=>date("Y")-120,
                    'maxYear'=>date("Y"),
                ]);
                echo $this->Form->input('estado_id', ['options' => $estados,'class'=>'form-control','empty'=>'Seleccione']);
                echo $this->Form->input('municipio_id', ['class'=>'form-control','empty'=>'Seleccione']);
                echo $this->Form->input('parroquia_id', ['class'=>'form-control','empty'=>'Seleccione']);
            ?>
            <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-primary']) ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>
<?= $this->element('estados-js')?>
