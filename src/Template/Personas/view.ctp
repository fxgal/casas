<div class="col-md-10 col-md-offset-1 col-sm-12 fondo_blanco">
    <button type="button" class="btn btn-default" onclick="history.back()">Volver</button>
    <h3><?= h($persona->nombre_simple) ?></h3>
    <table class="vertical-table table table-condensed table-striped table-responsive">
        <tr>
            <th scope="row"><?= __('Nacionalidad') ?></th>
            <td><?= h($persona->nacionalidad) ?></td>
            <th scope="row"><?= __('Cedula') ?></th>
            <td><?= $this->Number->format($persona->cedula) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nacimiento') ?></th>
            <td><?= $this->Fechas->getFechaFormat($persona->nacimiento) ?></td>
            <th scope="row"><?= __('Edad') ?></th>
            <td><?= $this->Fechas->getEdad($persona->nacimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($persona->nombre) ?></td>
            <th scope="row"><?= __('Segundo Nombre') ?></th>
            <td><?= h($persona->nombre_1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Apellido') ?></th>
            <td><?= h($persona->apellido) ?></td>
            <th scope="row"><?= __('Segundo Apellido') ?></th>
            <td><?= h($persona->apellido_1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ubicación') ?></th>
            <td colspan="3"><?= $persona->lugar_nacimiento ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sexo') ?></th>
            <td><?= $persona->sexo ? __('Femenino') : __('Masculino'); ?></td>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $persona->has('user') ? $this->Html->link($persona->user->email, ['controller' => 'Users', 'action' => 'view', $persona->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Registrado') ?></th>
            <td><?= h($persona->created) ?></td>
            <th scope="row"><?= __('Modificado') ?></th>
            <td><?= h($persona->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Contactos') ?></h4>
        <?php if (!empty($persona->contactos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Persona Id') ?></th>
                <th scope="col"><?= __('Movil') ?></th>
                <th scope="col"><?= __('Casa') ?></th>
                <th scope="col"><?= __('Twitter') ?></th>
                <th scope="col"><?= __('Facebook') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($persona->contactos as $contactos): ?>
            <tr>
                <td><?= h($contactos->id) ?></td>
                <td><?= h($contactos->persona_id) ?></td>
                <td><?= h($contactos->movil) ?></td>
                <td><?= h($contactos->casa) ?></td>
                <td><?= h($contactos->twitter) ?></td>
                <td><?= h($contactos->facebook) ?></td>
                <td><?= h($contactos->email) ?></td>
                <td><?= h($contactos->created) ?></td>
                <td><?= h($contactos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Contactos', 'action' => 'view', $contactos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Contactos', 'action' => 'edit', $contactos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Contactos', 'action' => 'delete', $contactos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Fotospersonas') ?></h4>
        <?php if (!empty($persona->fotospersonas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Persona Id') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Urlmin') ?></th>
                <th scope="col"><?= __('Active') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($persona->fotospersonas as $fotospersonas): ?>
            <tr>
                <td><?= h($fotospersonas->id) ?></td>
                <td><?= h($fotospersonas->persona_id) ?></td>
                <td><?= h($fotospersonas->url) ?></td>
                <td><?= h($fotospersonas->urlmin) ?></td>
                <td><?= h($fotospersonas->active) ?></td>
                <td><?= h($fotospersonas->created) ?></td>
                <td><?= h($fotospersonas->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Fotospersonas', 'action' => 'view', $fotospersonas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Fotospersonas', 'action' => 'edit', $fotospersonas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Fotospersonas', 'action' => 'delete', $fotospersonas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fotospersonas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
