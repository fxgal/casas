<div class="col-md-10 col-xs-12 col-md-offset-1">
    <div class="col-md-10 col-md-offset-1 col-sm-12 fondo_blanco">
        <h3><?= $titular->nombre_simple ?><!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#cargaFamiliar">Agregar Familiar</button></h3>
        <table cellpadding="0" cellspacing="0" class="table table-responsive table-condensed table-striped" id="personas_index">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nacionalidad</th>
                    <th>Cedula</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Parentesco</th>
                    <th>Edad</th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($familiares as $familiar): ?>
                <tr>
                    <td><?= $this->Number->format($familiar->id) ?></td>
                    <td><?= h($familiar->nacionalidad) ?></td>
                    <td><?= $this->Number->format($familiar->cedula) ?></td>
                    <td><?= h($familiar->nombre) ?></td>
                    <td><?= h($familiar->apellido) ?></td>
                    <td><?= $familiar->parentesco; ?></td>
                    <td><?= $this->Fechas->getEdad($familiar->nacimiento) ?></td>
                    <td class="actions">
                        <?= $this->element('actions',['dato'=>$familiar])?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!-- Ventana modal para el formulario -->
    <!-- Modal -->
    <div id="cargaFamiliar" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Carga Familiar</h4>
          </div>
          <div class="modal-body">
              <?= $this->Form->create($persona) ?>
              <fieldset>
                  <legend><?= __('Agregando carga familiar') ?></legend>
                  <div class="col-md-6 col-xs-12">
                      <?php
                          echo $this->Form->input('parentesco',['options'=>$this->Personas->getParentesco(),'class'=>'form-control']);
                          echo $this->Form->input('nombre',['class'=>'form-control']);
                          echo $this->Form->input('nombre_1',['class'=>'form-control','label'=>'Segundo Nombre']);
                          echo $this->Form->input('apellido',['class'=>'form-control']);
                          echo $this->Form->input('apellido_1',['class'=>'form-control','label'=>'Segundo Apellido']);
                      ?>
                  </div>
                  <div class="col-md-6 col-xs-12">
                      <div class="col-md-6 col-sm-12">
                          <?php echo $this->Form->input('nacionalidad',['class'=>'form-control','options'=>['Venezolano'=>'Venezolano','Extrangero'=>'Extrangero']]); ?>
                      </div>
                      <div class="col-md-6 col-sm-12">
                          <?php echo $this->Form->hidden('id'); ?>
                          <?php echo $this->Form->input('cedula',['class'=>'form-control']); ?>
                      </div>
                      <label>Sexo</label>
                      <?php
                          echo $this->Form->radio('sexo',['0'=>'Masculino','1'=>'Femenino']);
                          ?>
                          <label for="nacimiento">Fecha de Nacimiento</label>
                          <?php
                          echo $this->Form->input('nacimiento',[
                              'class'=>'form-control',
                              'label'=>'Nacimiento',
                              'minYear'=>date("Y")-120,
                              'maxYear'=>date("Y"),
                              'label'=>false
                          ]);
                          echo $this->Form->input('estado_id', ['options' => $estados,'class'=>'form-control','empty'=>'Seleccione','label'=>'Lugar de Nacimiento']);
                          echo $this->Form->input('municipio_id', ['class'=>'form-control','empty'=>'Seleccione','label'=>false]);
                          echo $this->Form->input('parroquia_id', ['class'=>'form-control','empty'=>'Seleccione','label'=>false]);
                      ?>
                      <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-primary']) ?>
                  </div>
              </fieldset>
              <?= $this->Form->end() ?>
          </div>
        </div>

      </div>
    </div>
    <!-- Fin ventana modal -->
</div>
<?= $this->element('estados-js')?>
<?= $this->element('datatable',['table_id'=>'personas_index']); ?>
