<?php
    $loguser = $this->request->session()->read('Auth.User');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Casas:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <!-- Bootstrap -->
    <?= $this->Html->css('/libs/bootstrap/dist/css/bootstrap.min.css') ?>
    <!-- Font Awesome -->
    <?= $this->Html->css('/libs/font-awesome/css/font-awesome.min.css') ?>
    <!-- dataTables -->
    <?= $this->Html->css('/libs/datatables.net-bs/css/dataTables.bootstrap.min.css')?>
    <!-- Propio-->
    <?= $this->Html->css('propio.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
    <!-- top navigation -->
        <?php if(!empty($loguser)){ ?>
        <?= $this->element('top_nav',['loguser'=>$loguser]); ?>
        <?php } ?>
        <!-- /top navigation -->
    <?= $this->Flash->render() ?>
    <div class="container">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
    <!-- jQuery -->
    <?= $this->html->script('/libs/jquery/dist/jquery.min.js'); ?>
    <!-- dataTables -->
    <?= $this->Html->script('/libs/datatables.net/js/jquery.dataTables.min.js'); ?>
    <?= $this->Html->script('/libs/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>
    <?= $this->fetch('datatables') ?>
    <!-- Bootstrap -->
    <?= $this->html->script('/libs/bootstrap/dist/js/bootstrap.min.js'); ?>
    <?= $this->fetch('script') ?>
</body>
<footer>
    Trabajadores de la Universidad Nacional Experimental Rómulo Gallegos<br>
    Desarrollado por Prof. Félix Galindo con <a href="https://cakephp.org/">CakePHP</a><br>
    Logo de <a href="https://openclipart.org">openclipart</a>
</footer>
</html>
