<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property \Cake\I18n\Time $access
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Noticia[] $noticias
 * @property \App\Model\Entity\Persona[] $personas
 * @property \App\Model\Entity\Rol[] $rols
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

/*    protected function _getNacionalidad($n)
    {
        return $n==0?'Venezolano':'Extranjero';
    }

    protected function _getNacionalidadOpciones($n)
    {
        return ['Venezolano','Extranjero'];
    }
    protected function _getSexo($n)
    {
        return $n==0?'Masculino':'Femenino';
    }

    protected function _getSexoOpciones($n)
    {
        return ['Masculino','Femenino'];
    }*/
}
