<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Persona Entity
 *
 * @property int $id
 * @property string $nacionalidad
 * @property int $cedula
 * @property string $nombre
 * @property string $nombre_1
 * @property string $apellido
 * @property string $apellido_1
 * @property bool $sexo
 * @property \Cake\I18n\Time $nacimiento
 * @property int $estado_id
 * @property int $municipio_id
 * @property int $parroquia_id
 * @property int $contacto_id
 * @property int $representante_id
 * @property int $user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Municipio $municipio
 * @property \App\Model\Entity\Parroquia $parroquia
 * @property \App\Model\Entity\Contacto[] $contactos
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Fotospersona[] $fotospersonas
 */
class Persona extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected function _getNombreConCedula()
    {
        return 'C. I. '. $this->_properties['cedula'].' - '.$this->_properties['nombre']. ' ' .$this->_properties['apellido'];
    }

    protected function _getNombreSimple()
    {
        return $this->_properties['nombre'] . ' ' .$this->_properties['apellido'];
    }

    protected function _getNombreCompleto()
    {
        return $this->_properties['nombre'] . ' ' .$this->_properties['nombre_1']. ' ' .$this->_properties['apellido']. ' ' .$this->_properties['apellido_1'];
    }
    protected function _getLugarNacimiento()
    {
        return $this->_properties['parroquia']['parroquia'] . ', Municipio ' .$this->_properties['municipio']['municipio']. ', Edo. ' .$this->_properties['estado']['estado'];
    }
}
