<?php
namespace App\View\Helper;
use Cake\View\Helper;

class LocalidadHelper extends Helper {
    var $helpers = ['Form'];
    public function getLocalidad($estados = null) {
      echo $this->Form->input('estado_id', ['options' => $estados,'empty' => 'Seleccione...']);
      echo $this->Form->input('municipio_id', ['empty' => 'Seleccione...']);
      echo $this->Form->input('parroquia_id', ['empty' => 'Seleccione...']);
    }

}
