<?php
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class PersonasHelper extends Helper {

    public function representante($parent_id) {
        $personas = TableRegistry::get('Personas');
        $representante = $personas->get($parent_id);
        return $representante;
    }

    public function getParentesco() {
        $parentesco=[
            'Hijo'=>'Hijo',
            'Hija'=>'Hija',
            'Padre'=>'Padre',
            'Madre'=>'Madre',
            'Esposa'=>'Esposa',
            'Esposo'=>'Esposo',
            'Concubina'=>'Concubina',
            'Concubino'=>'Concubino',
        ];
        return $parentesco;
    }
}
