<?php
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\I18n\Time;

class FechasHelper extends Helper {

    public function getFechaFormat($fecha,$format='dd-MM-yyyy') {
        $time = new Time($fecha);
        return $time->i18nFormat($format);
    }

    public function getEdad($fecha) {
        $time = new Time($fecha);
        $now = Time::now();
        return $time->diffInYears($now);
    }
}
