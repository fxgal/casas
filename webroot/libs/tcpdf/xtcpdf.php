<?php

require_once ROOT.DS. 'vendor' .DS. 'tcpdf' .DS. 'tcpdf.php';

class XTCPDF extends TCPDF {

    var $xheadertext = 'PDF creado using CakePHP y TCPDF';
    var $xheadercolor = array(0, 0, 200);
    //var $xfootertext = 'Copyright © %d XXXXXXXXXXX. All rights reserved.';
    var $xfooterfont = PDF_FONT_NAME_MAIN;
    var $xfooterfontsize = 10;
    var $font = 'freesans';
    var $area, $direccion, $departamento, $carrera, $asignatura;

    function setDatos($area, $direccion, $departamento, $carrera,$asig) {
        $this->area = $area;
        $this->direccion = $direccion;
        $this->departamento = $departamento;
        $this->carrera = $carrera;
        $this->asignatura=$asig;
        $this->SetMargins(20, 20);
    }

    function Header() {
        //$this->SetHeaderMargin(10);
        list($r, $b, $g) = $this->xheadercolor;
        //$this->setY(10);
        $this->SetFillColor($r, $b, $g);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 12, '', 0, 1, 'C', 1);
        //$this->Text(15, 26, $this->xheadertext);
        $this->SetFont($this->font, 'B', 10);
        $this->Image(WWW_ROOT . 'img/logounerg.jpeg', 20, 12, 50, 25);
        $this->Image(WWW_ROOT . 'img/logo.jpg', 240, 10, 30, 30);
        //$this->Ln(10);

        $this->Cell(0, 0, 'UNIVERSIDAD NACIONAL EXPERIMENTAL ROMULO GALLEGOS', 0, 1, 'C');
        $this->Cell(0, 0, $this->area['nombre'], 0, 1, 'C');
        $this->Cell(0, 0, $this->carrera['nombre'], 0, 1, 'C');
        $this->Cell(0, 0, $this->direccion['nombre'], 0, 1, 'C');
        $this->Cell(0, 0, $this->departamento['nombre'], 0, 1, 'C');
        $this->Ln(4);
        $this->SetFontSize(8);
        $this->Cell(0, 0, 'San Juan de Los Morros, ' . date("d/m/Y H:i:s"), 0, 1, 'R');
        $this->Ln(10);

    }

    function Footer() {
        $this->SetFontSize(8);
        $this->SetY(-40);
//        $this->Cell(35, 15, '', 0, 0, 'C', 0);
//        $this->Cell(45, 15, 'DECANO', 1, 0, 'C', 0);
//        $this->Cell(115, 15, 'FIRMAS Y SELLOS', 0, 0, 'C', 0);
//        $this->Cell(50, 15, 'DIRECTOR', 1, 0, 'C');
//        $this->Cell(30, 15, '', 0, 0, 'C', 0);
        $this->Ln();
        $this->Cell(0, 4, $this->asignatura['nombre'], 0, 1, 'C');
        $this->Cell(0, 4, "Universidad Nacional Experimental Rómulo Gallegos, RIF.: G-20003225-1", 'T', 1, 'C', 0);
        $this->Cell(0, 4, "Ciudad universitaria, via El Castrero - San Juan de los Morros - Estado Guárico - Venezuela", 0, 1, 'C', 0);
        $this->Cell(0, 4, "Telfs: (0246) 431.05.84 - 431.08.31 - 431.27.23 . Fax: (0246) 431.26.70 Ext. 157 - 158", 0, 1, 'C', 0);
    }

    function CodVal() {
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; //posibles caracteres a usar
        $numerodeletras = 10; //numero de letras para generar el texto
        $cadena = ""; //variable para almacenar la cadena generada
        for ($i = 0; $i < $numerodeletras; $i++) {
            $cadena .= substr($caracteres, rand(0, strlen($caracteres)), 1); /* Extraemos 1 caracter de los caracteres
              entre el rango 0 a Numero de letras que tiene la cadena */
        }
        return $cadena;
    }

}
?>
