<?php

require_once WWW_ROOT.DS. 'libs' .DS. 'tcpdf' .DS. 'tcpdf.php';
setlocale(LC_ALL,"es_ES");
class Ficha extends TCPDF {

    var $xheadertext = 'PDF creado using CakePHP y TCPDF';
    var $xheadercolor = array(0, 0, 200);
    //var $xfootertext = 'Copyright © %d XXXXXXXXXXX. All rights reserved.';
    var $xfooterfont = PDF_FONT_NAME_MAIN;
    var $xfooterfontsize = 10;
    var $font = 'freesans';
    var $area, $direccion, $departamento, $carrera, $asignatura;

    function setDatos($area, $direccion, $departamento, $carrera,$asig) {
        $area= $direccion= $departamento= $carrera=$asig='A';
        $this->area = $area;
        $this->direccion = $direccion;
        $this->departamento = $departamento;
        $this->carrera = $carrera;
        $this->asignatura=$asig;
        $this->SetMargins(20, 20);
    }

    function Header() {
        list($r, $b, $g) = $this->xheadercolor;
        $this->Ln(10);
        $this->Cell(0, 1, '', 0, 1, 'C', 0);
        $this->SetFont('', 'B', 10);
        $this->Image(WWW_ROOT . 'img/logo-casa.png', 20, 20, 15, 15);

        $this->Cell(0, 0, "FEDERACIÓN VENEZOLANA DE BEISBOL", 0, 1, 'C');
        $this->Cell(0, 0, "AFILIADA A LA IBAF - COPABE - CONSUBE - COV", 0, 1, 'C');
        $this->Cell(0, 0, "INSCRITA EN EL INSTITUTO NACIONAL DE DEPORTES", 0, 1, 'C');
        $this->Ln(5);
        //$this->Cell(0, 0, "FICHA NACIONAL", 0, 1, 'C');
        $this->Ln(5);

    }

    function Footer() {
        $this->SetFontSize(8);
        $this->SetY(-40);
//        $this->Cell(35, 15, '', 0, 0, 'C', 0);
//        $this->Cell(45, 15, 'DECANO', 1, 0, 'C', 0);
//        $this->Cell(115, 15, 'FIRMAS Y SELLOS', 0, 0, 'C', 0);
//        $this->Cell(50, 15, 'DIRECTOR', 1, 0, 'C');
//        $this->Cell(30, 15, '', 0, 0, 'C', 0);
        //$this->Ln();
        //$this->Cell(0, 4, $this->asignatura['nombre'], 0, 1, 'C');
        //$this->Cell(0, 4, "Universidad Nacional Experimental Rómulo Gallegos, RIF.: G-20003225-1", 'T', 1, 'C', 0);
        //$this->Cell(0, 4, "Ciudad universitaria, via El Castrero - San Juan de los Morros - Estado Guárico - Venezuela", 0, 1, 'C', 0);
        //$this->Cell(0, 4, "Telfs: (0246) 431.05.84 - 431.08.31 - 431.27.23 . Fax: (0246) 431.26.70 Ext. 157 - 158", 0, 1, 'C', 0);
    }

    function CodVal() {
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; //posibles caracteres a usar
        $numerodeletras = 10; //numero de letras para generar el texto
        $cadena = ""; //variable para almacenar la cadena generada
        for ($i = 0; $i < $numerodeletras; $i++) {
            $cadena .= substr($caracteres, rand(0, strlen($caracteres)), 1); /* Extraemos 1 caracter de los caracteres
              entre el rango 0 a Numero de letras que tiene la cadena */
        }
        return $cadena;
    }

}
?>
